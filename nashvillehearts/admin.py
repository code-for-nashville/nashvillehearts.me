from django.contrib import admin
from django.utils.html import format_html

from . import models


# Register your admin models
@admin.register(models.Provider)
class ProviderAdmin(admin.ModelAdmin):
    list_display = ('name', 'org_type', 'email', 'last_login', 'is_faith_based', 'is_active')
    ordering = ('name', 'account__last_login')
    list_filter = ('org_type',)
    search_fields = ('name', 'locations__name')
    filter_horizontal = ('locations', 'services', 'client_types')


@admin.register(models.Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ('name', 'address1', 'city', 'state', 'zip')
    ordering = ('name',)
    search_fields = ('name', 'address1')


@admin.register(models.Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('name', 'icon')
    ordering = ('name',)
    search_fields = ('name',)


@admin.register(models.ClientType)
class ClientTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    ordering = ('name',)
    search_fields = ('name',)


@admin.register(models.Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('title', 'start_datetime', 'end_datetime', 'provider', 'event_type',
                    'event_address1', 'event_city', 'event_state', 'event_zip')
    list_filter = ('event_type',)
    ordering = ('title',)
    search_fields = ('name', 'provider__name')


@admin.register(models.SavedSearch)
class SavedSearchAdmin(admin.ModelAdmin):
    list_display = ('id', 'search_parameters', 'modified', 'created')
    ordering = ('-modified',)
    search_fields = ('search_parameters',)


@admin.register(models.VisitorLanguage)
class VisitorLanguageAdmin(admin.ModelAdmin):
    list_display = ('language', 'iso_code')
    ordering = ('-language',)
    search_fields = ('language',)


@admin.register(models.Faq)
class FaqAdmin(admin.ModelAdmin):
    list_display = ('id', 'question', 'modified', 'created')
    ordering = ('question',)
    search_fields = ('question', 'answer', )
