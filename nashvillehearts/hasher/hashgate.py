# A class for performing simple 2-way hashing/encryption/decryption  ...not necessarily for security purposes
import hashlib
import json
import base64

from Crypto.Cipher import AES

from nashvillehearts.models import SavedSearch


class CipherGate:

    def __init__(self):
        init_word = "nashvilleheartsme".encode('utf-8')
        self.key = hashlib.sha256(init_word).digest()
        self.IV = 16 * '\x00'  # Initialization vector: simple ...not mission appropriate
        self.mode = AES.MODE_CBC
        self.encryptor = AES.new(self.key, self.mode, IV=self.IV)

    def encrypt(self, source, serialize_to_json=True):

        if type(source) != 'str':
            source = json.dumps(source, sort_keys=True)

        # pad the string with zeroes
        source = (source + (AES.block_size - len(str(source)) % AES.block_size) * "\0")
        encrypted = self.encryptor.encrypt(source)
        return base64.encodebytes(encrypted)

    def decrypt(self, target):

        # if type(target) != 'str':
        #     raise IOError("target provided is not an encrypted string")

        unencoded = base64.decodebytes(target)
        print(unencoded)

        return self.encryptor.decrypt(unencoded).rstrip("\0")


class ObjectHasher:

    def __init__(self):
        super(ObjectHasher, self).__init__()

    def stash(self, source):

        if type(source) != 'str':
            source = json.dumps(source, sort_keys=True)

        saved_search = SavedSearch.objects.create(search_parameters=source)

        return saved_search.id

    def unstash(self, uuid):

        try:
            obj = SavedSearch.objects.get(id=uuid)
            entry = json.loads(obj.search_parameters)
        except SavedSearch.DoesNotExist:
            entry = {}

        return entry
