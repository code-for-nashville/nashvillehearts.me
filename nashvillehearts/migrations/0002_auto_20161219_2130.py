# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-12-19 21:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nashvillehearts', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='state',
            field=models.CharField(choices=[('TN', 'Tennessee')], default='TN', max_length=2),
        ),
    ]
