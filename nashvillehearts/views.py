import datetime
import hashlib
import json
from django.shortcuts import render, redirect
from rest_framework import generics, permissions

from django.http import Http404
from django.db import IntegrityError
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in
from django.shortcuts import render
from django.contrib.auth import logout
from django.views.generic import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from django.conf import settings

from rest_framework.views import APIView
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
from rest_framework import status

from .models import Provider, Location, Event, UrgentNeed, Service, ClientType, VisitorLanguage, Faq
from .serializers import (ProviderSerializer, LocationSerializer,
                          EventSerializer, UrgentNeedSerializer,
                          ServiceSerializer, ClientTypeSerializer,
                          UserSerializer, VisitorLanguageSerializer,
                          FaqSerializer)
from .mailer.mailgate import MailGate
from .hasher.hashgate import ObjectHasher


# The Root View
def root(request):
    return render(request, 'index.html', {})


# 'Access-Control-Allow-Headers', 'Content-Type'


# Custom NHM Token Authenticate View
class NHMTokenAuthentication(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        response = super(NHMTokenAuthentication, self).post(request, *args, **kwargs)
        token = Token.objects.get(key=response.data['token'])
        try:
            user = User.objects.get(id=token.user_id)
        except User.DoesNotExist as err:
            print('Authentication failed retrieving user for id: {}'.format(token.user_id))

        # get the provider info
        provider_id = user.provider.id if user and hasattr(user, 'provider') else None
        provider_name = user.provider.name if user and hasattr(user, 'provider') else None

        # send signal to update user's last_login field since Token Auth doesn't do that automatically
        user_logged_in.send(sender=user.__class__, request=request, user=user)

        return Response({'token': token.key,
                         'user_id': token.user_id,
                         'provider_id': provider_id,
                         'provider_name': provider_name})


# User View
class UserRegisterView(generics.CreateAPIView):
    serializer_class = UserSerializer
    permission_classes = (permissions.AllowAny,)


# Provider Views
class ProviderList(generics.ListCreateAPIView):
    queryset = Provider.objects.all()
    serializer_class = ProviderSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    parser_classes = (JSONParser, FormParser, MultiPartParser)

    def get_queryset(self):
        """
        Optionally matches the returned providers to a search term
        by filtering against a `search` query parameter in the URL.
        """
        queryset = Provider.objects.all()
        search = self.request.query_params.get('search', None)
        service = self.request.query_params.get('service', None)
        client_type = self.request.query_params.get('clienttype', None)
        # by_location = self.request.query_params.get('bylocation', None)

        params = {
            'is_public': True,
        }

        if search:
            params['name__icontains'] = search
        if service:
            params['services'] = service
        if client_type:
            params['client_types'] = client_type

        queryset = queryset.filter(**params)

        """
        if search and service:
            queryset = queryset.filter(name__icontains=search, services=service)
        elif search:
            queryset = queryset.filter(name__icontains=search)
        elif service:
            queryset = queryset.filter(services=service)
        """

        return queryset


class ProviderDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Provider.objects.all()
    serializer_class = ProviderSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    parser_classes = (JSONParser, FormParser, MultiPartParser)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


# Service Views
class ServiceList(generics.ListAPIView):
    queryset = Service.objects.all().order_by('name')
    serializer_class = ServiceSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    parser_classes = (JSONParser, FormParser, MultiPartParser)


# ClientType Views
class ClientTypeList(generics.ListAPIView):
    queryset = ClientType.objects.all().order_by('name')
    serializer_class = ClientTypeSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    parser_classes = (JSONParser, FormParser, MultiPartParser)


# ClientType Views
class LanguageList(generics.ListAPIView):
    queryset = VisitorLanguage.objects.all().order_by('language')
    serializer_class = VisitorLanguageSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    parser_classes = (JSONParser, FormParser, MultiPartParser)


# Faq Views
class FaqList(generics.ListAPIView):
    queryset = Faq.objects.all().order_by('question')
    serializer_class = FaqSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    parser_classes = (JSONParser, FormParser, MultiPartParser)

    def get_queryset(self):
        """
        Optionally restricts the Faq questions to public/private.
        """
        queryset = Faq.objects.all().order_by('question')
        public = self.request.query_params.get('public', None)

        params = {}

        if public:
            params['is_public'] = True

        queryset = queryset.filter(**params)

        return queryset


# Location Views
class LocationList(generics.ListCreateAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    parser_classes = (JSONParser, FormParser, MultiPartParser)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        """
        Optionally matches the returned providers to a search term
        by filtering against a `search` query parameter in the URL.
        """
        queryset = Location.objects.all()
        search = self.request.query_params.get('search', None)
        service = self.request.query_params.get('service', None)

        if search and service:
            queryset = queryset.filter(provider__name__icontains=search,
                                       provider__services=service,
                                       provider__is_public=True).order_by('name')
        elif search:
            queryset = queryset.filter(provider__name__icontains=search,
                                       provider__is_public=True).order_by('name')
        elif service:
            queryset = queryset.filter(provider__services=service,
                                       provider__is_public=True).order_by('name')

        # add , account__is_active=True to all the above queries??

        return queryset

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class LocationDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    parser_classes = (JSONParser, FormParser, MultiPartParser)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


# Event Views
class EventList(generics.ListCreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    parser_classes = (JSONParser, FormParser, MultiPartParser)
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        """
        Optionally matches the returned providers to a search term
        by filtering against a `search` query parameter in the URL.
        """
        queryset = Event.objects.all()
        params = {'event_type': Event.GENERAL, 'start_datetime__gte': datetime.datetime.today()}
        search = self.request.query_params.get('search', None)
        provider_id = self.request.query_params.get('provider', None)
        start_date = self.request.query_params.get('start', None)
        end_date = self.request.query_params.get('end', None)

        print("start date: {}, type: {}".format(start_date, type(start_date)))

        if search:
            params['title__icontains'] = search
        if start_date:
            params['start_datetime__gte'] = start_date
        if end_date:
            params['end_datetime__lte'] = end_date
        if provider_id:
            params['provider'] = provider_id

        queryset = queryset.filter(**params).order_by('start_datetime')

        return queryset

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class EventDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    parser_classes = (JSONParser, FormParser, MultiPartParser)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


# UrgentNeed Views
class UrgentNeedList(generics.ListCreateAPIView):
    queryset = UrgentNeed.objects.all()
    serializer_class = UrgentNeedSerializer
    parser_classes = (JSONParser, FormParser, MultiPartParser)
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        """
        Optionally matches the returned providers to a search term
        by filtering against a `search` query parameter in the URL.
        """
        queryset = UrgentNeed.objects.all()
        params = {'is_fulfilled': False, 'event_type': UrgentNeed.URGENT_NEED,
                  'end_datetime__gte': datetime.datetime.today() - datetime.timedelta(hours=1)}
        # the end_datetime__gte above allows even items that are up to 1 hour over expiration time still appear in list
        search = self.request.query_params.get('search', None)
        up_to_date = self.request.query_params.get('end', None)
        provider_id = self.request.query_params.get('provider', None)

        if search:
            params['title__icontains'] = search
        if up_to_date:
            # today = datetime.datetime.today()
            params['end_datetime__lte'] = up_to_date
            # params['end_datetime__gte'] = today
        if provider_id:
            params['provider'] = provider_id

        queryset = queryset.filter(**params).order_by('end_datetime')

        return queryset

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class UrgentNeedDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = UrgentNeed.objects.all()
    serializer_class = UrgentNeedSerializer
    parser_classes = (JSONParser, FormParser, MultiPartParser)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


# Simple Views
@method_decorator(csrf_exempt, name='dispatch')
class ContactFormAjaxView(View):

    def add_access_control_headers(self, response):
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
        response["Access-Control-Max-Age"] = "1000"
        response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    def options(self, request, *args, **kwargs):
        response = JsonResponse()
        self.add_access_control_headers(response)
        return response

    def post(self, request, *args, **kwargs):
        resp = {}

        if request.POST:
            name = request.POST.get('name')
            email = request.POST.get('email')
            subject = request.POST.get('subject', None)
            message = request.POST.get('message')
            # organization = request.POST.get('organization', "n/a")

            if name and email and message:
                message = "Name: {}\r\n\r\nMessage:\r\n".format(name) + message
                mailer = MailGate()
                mailed, mesg = mailer.send_contact_email(email, message, name, subject=subject)
                resp['message'] = {
                    'mailed': mailed,
                    'caption': mesg
                }

        else:
            resp['error'] = 'No valid POST data found!'

        json_response = JsonResponse(resp)
        self.add_access_control_headers(json_response)
        return json_response

    def get(self, request, *args, **kwargs):
        resp = {}
        name = 'Simon Tester'
        email = 'syghon@gmail.com'
        message = 'This is a test message'
        organization = "SiggyWorks, LLC"
        if name and email and message:
            message = "Name: {}\r\n\r\nOrganization: {}\r\n\r\n".format(name, organization) + message
            mailer = MailGate()
            mailed, mesg = mailer.send_contact_email(email, message, name, subject="NHM Test Message")
            resp['message'] = {
                'mailed': mailed,
                'caption': mesg
            }
            resp['email'] = email
            resp['name'] = name

        json_response = JsonResponse(resp)
        self.add_access_control_headers(json_response)
        return json_response


@method_decorator(csrf_exempt, name='dispatch')
class ProviderAccessRequestView(View):

    def add_access_control_headers(self, response):
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
        response["Access-Control-Max-Age"] = "1000"
        response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    def options(self, request, *args, **kwargs):
        response = JsonResponse()
        self.add_access_control_headers(response)
        return response

    def post(self, request, *args, **kwargs):
        resp = {}

        if request.POST:
            organization = request.POST.get('organization')
            email = request.POST.get('email')
            phone = request.POST.get('phone')
            ein = request.POST.get('ein')
            website = request.POST.get('website')
            org_type = request.POST.get('org_type', None)
            contact = request.POST.get('contact', 'n/a')

            print([organization, email, phone, contact, '!!!'])

            if organization and email:
                org_types = [x[0] for x in Provider.ORGANIZATION_TYPES]
                mailer = MailGate()
                provider, created = self._create_provider_account(email,
                                                                  organization,
                                                                  contact_name=contact,
                                                                  phone=phone,
                                                                  website=website)

                if created:
                    if org_type in org_types:
                        provider.org_type = org_type
                        provider.save()

                    mailer.sendmail('ACCESS.REQUEST', settings.NHM_ADMIN_EMAIL,
                                    {'organization': organization,
                                     'organization_type': provider.org_type,
                                     'phone': phone,
                                     'email': email,
                                     'website': website,
                                     'ein': ein,
                                     'contact_name': contact
                                     }, request=self.request)
                    resp['message'] = {
                        'organization': organization,
                        'organization_type': provider.org_type,
                        'phone': phone,
                        'email': email,
                        'website': website,
                        'ein': ein,
                        'contact_name': contact
                    }
                else:
                    resp['message'] = "Account already exists {} ({})".format(organization, email)

        else:
            resp['error'] = 'No valid POST data found!'

        json_response = JsonResponse(resp)
        self.add_access_control_headers(json_response)
        return json_response

    def get(self, request, *args, **kwargs):
        resp = {}
        name = 'Simon Tester'
        email = 'syghon@gmail.com'
        message = 'This is a test message'
        organization = "SiggyWorks, LLC"
        if name and email and message:
            message = "Name: {}\r\n\r\nOrganization: {}\r\n\r\n".format(name, organization) + message
            mailer = MailGate()
            mailed, mesg = mailer.send_contact_email(email, message, name, subject="NHM Test Provider Request")
            resp['message'] = {
                'mailed': mailed,
                'caption': mesg
            }
            resp['email'] = email
            resp['name'] = name

        json_response = JsonResponse(resp)
        self.add_access_control_headers(json_response)
        return json_response

    def _create_provider_account(self, email, organization, contact_name=None, phone=None, website=None):
        params = {
            'username': email,
            'email': email
        }
        if contact_name:
            name_parts = contact_name.split(' ')
            params['first_name'] = name_parts[0]
            params['last_name'] = name_parts[-1]

        new_user, created = User.objects.get_or_create(**params)

        if created:
            provider = Provider.objects.create(account=new_user, name=organization, phone=phone, website=website)
            new_user = provider

        return new_user, created


@method_decorator(csrf_exempt, name='dispatch')
class ProviderFileUploadAjaxView(View):

    def add_access_control_headers(self, response):
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
        response["Access-Control-Max-Age"] = "1000"
        response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    def options(self, request, *args, **kwargs):
        response = JsonResponse()
        self.add_access_control_headers(response)
        return response

    def post(self, request, *args, **kwargs):
        resp = {}

        if request.POST:
            provider_id = request.POST.get('provider_id')
            logo_file = request.FILES.get('logo_file')

            if Provider.objects.filter(id=provider_id).exists() and logo_file:
                provider = Provider.objects.get(id=provider_id)
                provider.avatar = logo_file
                provider.save()
                resp['message'] = "Logo uploaded successfully for {}".format(provider.name)
                resp['logo_url'] = provider.avatar.url
            else:
                resp['error'] = "Bad provider ID ({}) or no logo file found".format(provider_id)

        else:
            resp['error'] = 'No valid POST/FILE data found!'

        json_response = JsonResponse(resp)
        self.add_access_control_headers(json_response)
        return json_response


@method_decorator(csrf_exempt, name='dispatch')
class ProviderCredentialUpdateView(View):

    def add_access_control_headers(self, response):
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
        response["Access-Control-Max-Age"] = "1000"
        response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    def options(self, request, *args, **kwargs):
        response = JsonResponse()
        self.add_access_control_headers(response)
        return response

    def post(self, request, *args, **kwargs):
        resp = {}

        if request.POST:
            provider_id = request.POST.get('provider')
            current_pass = request.POST.get('old')
            new_pass = request.POST.get('new')
            confirm_pass = request.POST.get('confirm')

            if new_pass == confirm_pass:
                provider_qs = Provider.objects.filter(id=provider_id)
                if provider_qs.exists():
                    provider = provider_qs.first()
                    # check current_pass
                    if provider.account.check_password(current_pass):
                        # updated password
                        provider.account.set_password(new_pass)
                        provider.account.save()
                        resp['message'] = "Password updated successfully for {} ({})".format(provider.name, provider.email)
                    else:
                        resp['error'] = "Wrong current password given!"
                else:
                    resp['error'] = "Bad provider ID ({}) given".format(provider_id)
            else:
                resp['error'] = "Passwords do not match!"

        else:
            resp['error'] = 'No valid POST/FILE data found!'

        json_response = JsonResponse(resp)
        self.add_access_control_headers(json_response)
        return json_response


@method_decorator(csrf_exempt, name='dispatch')
class BookmarkEncodeView(View):

    def add_access_control_headers(self, response):
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
        response["Access-Control-Max-Age"] = "1000"
        response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    def options(self, request, *args, **kwargs):
        response = JsonResponse()
        self.add_access_control_headers(response)
        return response

    def post(self, request, *args, **kwargs):
        cipher = ObjectHasher()
        resp = {}

        if request.POST:
            post_as_json = json.dumps(request.POST, sort_keys=True)
            uuid = cipher.stash(post_as_json)
            resp['bookmark'] = uuid
        else:
            resp['error'] = 'No valid POST/FILE data found!'

        json_response = JsonResponse(resp)
        self.add_access_control_headers(json_response)
        return json_response


@method_decorator(csrf_exempt, name='dispatch')
class BookmarkDecodeView(View):

    def add_access_control_headers(self, response):
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
        response["Access-Control-Max-Age"] = "1000"
        response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    def options(self, request, *args, **kwargs):
        response = JsonResponse()
        self.add_access_control_headers(response)
        return response

    def post(self, request, *args, **kwargs):
        cipher = ObjectHasher()
        resp = {}

        if request.POST:
            bookmark = request.POST.get('bookmark', None)

            if bookmark:
                # reverse the hash
                params = cipher.unstash(bookmark)
                resp = params if type(params) == 'dict' else json.loads(params)
            else:
                resp['error'] = 'Error: no bookmark provided'

            # resp['bookmark'] = post_as_md5
        else:
            resp['error'] = 'No valid POST/FILE data found!'

        json_response = JsonResponse(resp)
        self.add_access_control_headers(json_response)
        return json_response
