# Helper Class to handle environment variables


class Environ(object):

    def __init__(self, config_file):
        self.config_file = config_file
        self.VARS = {}

        self._load_config(self.config_file)

    def _load_config(self, file):
        with open(file, 'r') as cf:
            lines = [l.rstrip("\n").strip() for l in cf.readlines()]

            for line in lines:
                if not line.startswith('#'):
                    key, sep, val = line.partition("=")
                    key = key.strip()
                    val = val.strip()
                    self.VARS[key] = val

    def get_variable(self, key):
        return self.VARS[key]  # Let it throw an error if key doesn't exist

