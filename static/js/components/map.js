/**
 * Created by sada on 12/29/16.
 */

import React from 'react';
import ReactDOM from 'react-dom';

export default class Map extends React.Component {
    constructor(props) {
        super(props);

        const {lat, lng} = this.props.initialCenter;

        this.state = {
            currentLocation: {
                lat: lat,
                lng: lng
            },
            //map: null,
            markers: null
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.google !== this.props.google) {
            this.loadMap();
        }
    }

    componentDidMount() {
        this.loadMap();
    }

    renderChildren() {
        if(this && this.props){
            const {children} = this.props;
            console.log("Map children count: ", children.length);

            if(!children) {
                console.log("No Map children: ", children);
                return;
            }

            //console.log("renderChild: map ...", this.map);
            //console.log("renderChild: google...", this.props.google);

            return React.Children.map(children, c => {
                return React.cloneElement(c, {
                    map: this.map,
                    google: this.props.google,
                    mapCenter: this.state.currentLocation
                });
            });
        }
        console.log("No props ...confused ...bailing!");
    }

    loadMap() {
        if (this.props && this.props.google) {
            // google is available
            const {google} = this.props;
            const maps = google.maps;

            const mapRef = this.refs.map;
            const node = ReactDOM.findDOMNode(mapRef);

            const evtNames = ['ready', 'click', 'dragend'];

            let {zoom} = this.props;
            const {lat, lng} = this.state.currentLocation;
            const center = new maps.LatLng(lat, lng);
            const mapConfig = Object.assign({}, {
                center: center,
                zoom: zoom
            });
            this.map = new maps.Map(node, mapConfig);

            evtNames.forEach(
                (evt) => {
                    // add the handler
                    this.map.addListener(evt, this.handleEvent(evt));

                    // add the propTypes
                    Map.PropTypes[camelize(evt)] = React.PropTypes.func;
                }
            );

            this.setState({markers: this.renderChildren()});

            maps.event.trigger(this.map, 'ready');
        }
    }

    handleEvent(evtName) {
        let handlerName = "on"+camelize(evtName);

        // call the handler which should be on the props object
        return (e) => {
            if(this.props[handlerName]) {
                this.props[handlerName](this.props, this.map, e);
            } else {
                console.log('Could not find handler: ' + handlerName);
            }
        };

    }

    render() {
        const style = {
            position: 'static',
            height: '100%',
            width: '100%',
        };

        return (
            <div ref='map' style={style}>
                Loading map...
                {this.state.markers}
            </div>
        )
    }
}

Map.PropTypes = {
    google: React.PropTypes.object,
    zoom: React.PropTypes.number,
    initialCenter: React.PropTypes.object
};

Map.defaultProps = {
    zoom: 12,
    // Nashville
    initialCenter: {
        lat: 36.174465,
        lng: -86.767960,
    },
    onDragend: (props, map, e) => {
        console.log("map was dragged:", e);
    },
    onReady: (props, map, e) => {
        console.log("map is ready:", map, e);
    },
    onClick: (props, map, e) => {
        console.log("map was clicked: ", props, map, e);
    }
};


// Helper functions
        function camelize(str) {
            return str.split(' ').map(function(word){
                return word.charAt(0).toUpperCase() + word.slice(1);
            }).join('');
        }