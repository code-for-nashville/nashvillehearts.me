/**
 * Created by sada on 12/31/16.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import ReactDOMServer from 'react-dom/server'

export default class InfoWindow extends React.Component {

    constructor(props, context) {
        super(props, context);
    }


    render() {
        return null;
    }

    componentDidMount() {
        this.renderInfoWindow();
    }

    componentDidUpdate(prevProps) {
        if( prevProps.map !== this.props.map ) {
            console.log("about to re-render a marker...");
            this.renderInfoWindow();
        }
        //console.log("...end of this marker attemp...");

        if (this.props.children !== prevProps.children) {
          this.updateContent();
        }

        if (this.props.visible !== prevProps.visible) {
            this.props.visible ? this.openInfoWindow() : this.closeInfoWindow();
            console.log("infowindow is visible?", this.props.visible);
        }
    }

    componentWillUnmount() {

    }

    updateContent() {
        const content = this.renderChildren();
        this.infowindow.setContent(content);
    }

    renderInfoWindow() {
        let {map, google, mapCenter} = this.props;

        this.infowindow = new google.maps.InfoWindow({
          content: ''
        });
        const iw = this.infowindow;
        console.log("infowindow: ", this.infowindow);

        google.maps.event.addListener(iw, 'closeclick', this.onInfoWindowClose.bind(this));
        google.maps.event.addListener(iw, 'domready', this.onInfoWindowOpen.bind(this));
    }

    renderChildren() {
        const {children} = this.props;
        return ReactDOMServer.renderToString(children);
    }

    openInfoWindow() {
        this.infowindow.open(this.props.map, this.props.marker);
        console.log("opened infowindow? ", this.infowindow);
    }
    closeInfoWindow() {
        //this.infowindow.close();
        console.log("closed infowindow? ", this.infowindow);
    }

    onInfoWindowOpen() {
        if (this.props.onOpen) this.props.onOpen();
    }
    onInfoWindowClose() {
        if (this.props.onClose) this.props.onClose();
    }

}
