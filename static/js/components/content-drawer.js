/**
 * Created by sada on 12/30/16.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Subheader from 'material-ui/Subheader';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import CommunicationChatBubble from 'material-ui/svg-icons/communication/chat-bubble';


const styles = {
    wrapper: {
        padding: "20px 10px 10px",
        textAlign: "center",
        color: '#777777',
        textColor: '#777777'
    },
    subheader: {
        color: '#676767'
    },
    textfield: {
        color: '#777777'
    },
    button: {
        color: '#dddddd'
    },
    selectfield: {
        textAlign: 'left'
    },
    paper: {
        margin: 20,
        marginBottom: 40,
        textAlign: 'center',
        display: 'block',
    }
};

export default class ContentDrawer extends React.Component {

    constructor(props, context) {
        super(props, context);

        const defaultDate = new Date();
        defaultDate.setHours(0, 0, 0, 0);

        this.state = {
            defaultDate: defaultDate,
            autoOk: true,
            value: '',
            services: [],
            service: null,
            clientType: null,
            disableYearSelection: false,
        };
    }

    componentDidMount() {
        console.log("...ContentDrawer DidMount...");
        // <MenuItem value={1} primaryText="Never" />
        if(this.props.drawer.toUpperCase() === 'RESOURCES') {
            jQuery.ajax({
                url: '/resources/services',
                datatype: 'json',
                cache: false,
                success: function(data) {
                    this.setState({services: data});
                    console.log("services --> ", data);
                }.bind(this)
            });
        }
    }

    componentDidUpdate(prevProps){

    }

    handleUserSearch = () => {
        switch(this.props.drawer.toUpperCase()){
            case 'RESOURCES':
                console.log('search resources');
                this.props.contentHandler({
                    contentType: 'provider',
                    search: 'sil',
                    service: 88
                });
                break;
            case 'EVENTS':
                console.log('find events');
                this.props.contentHandler({
                    contentType: 'event',
                    search: '',
                    startDate: null,
                    endDate: null
                });
                break;
            case 'URGENT NEEDS':
                console.log('find urgent needs');
                this.props.contentHandler({
                    contentType: 'need',
                    search: '',
                    endDate: null
                });
                break;
            default:
                break;
        }
    }

    renderDrawerControls = () => {
        if(!this.props.drawer) {
            return <span>No content</span>;
        }

        if(this.props.drawer.toUpperCase() === 'RESOURCES') {
            let serviceItems = [];

            if(this.props.services) {
                serviceItems = this.props.services.map(function(serv){
                    return <MenuItem key={serv.id} value={serv.id} primaryText={serv.name}/>
                });
                serviceItems.unshift(<MenuItem key={0} value={null} primaryText="" />);
            }

            return (
                <div>
                    <TextField style={styles.textfield}
                               hintText="Search Term"
                               floatingLabelText="Search"/>
                    <SelectField id={'sv_id'}
                      floatingLabelText="Service Type"
                      value={this.state.service}
                      onChange={this.handleServiceEvent} style={styles.selectfield}
                    >
                        {serviceItems}
                    </SelectField>
                    <RaisedButton onClick={this.handleUserSearch} style={styles.button} label="Find Providers" primary={true} />
                </div>
            );
            //fullWidth={true}

        } else if(this.props.drawer.toUpperCase() === 'EVENTS') {
            return (
                <div>
                    <TextField style={styles.textfield}
                               hintText="Event Name/Title"
                               floatingLabelText="Event Name"/>
                    <DatePicker
                        id={'stdt_id'}
                        onChange={this.handleStartDateEvent}
                        autoOk={this.state.autoOk}
                        floatingLabelText="Start Date"
                        locale={'en-US'}
                        defaultDate={this.state.defaultDate} style={styles.textfield}
                    />
                    <DatePicker
                        id={'etdt_id'}
                        onChange={this.handleEndDateEvent}
                        autoOk={this.state.autoOk}
                        locale={'en-US'}
                        floatingLabelText="End Date" style={styles.textfield}
                    /><br/>
                    <RaisedButton style={styles.button} label="Find Events" primary={true} />
                </div>
            );

        } else if(this.props.drawer.toUpperCase() === 'URGENT NEEDS') {
            return (
                <div>
                    <TextField style={styles.textfield}
                               hintText="Urgent Need"
                               floatingLabelText="Urgent Need"/>
                    <DatePicker
                        id={'uetdt_id'}
                        onChange={this.handleUrgentDateEvent}
                        autoOk={this.state.autoOk}
                        locale={'en-US'}
                        floatingLabelText="End Date/Time" style={styles.textfield}
                    /><br/>
                    <RaisedButton style={styles.button} label="Find Urgent Needs" primary={true} />
                </div>
            );
        } else {
            return <span>No information available!</span>
        }
    }

    render() {
        let dataList = null;
        let controls = this.renderDrawerControls();

        if(this.props.data){
            let pdata = JSON.stringify(this.props.data);
            dataList = <div> {this.props.data} <br/><br/> {pdata} </div>

        } else {
            dataList = <div> ...some data goes here...</div>
        }
        return (
            <div style={styles.wrapper}>
                {controls}
                {dataList}
            </div>
        );
    }

    componentDidMount() {
        console.log("...ContentDrawer DidMount...");
    }

    componentDidUpdate(prevProps) {
        console.log("...ContentDrawer DidUpdate...");
    }

    componentWillUnmount() {
        console.log("...ContentDrawer WillUnmount...");
    }

    handleServiceEvent = (event, index, value) => {
        console.log(event, index, value);
        this.setState({ service: value });
    };
    handleStartDateEvent = (event, date) => {
        console.log(event, date);
        this.setState({ startDate: date });
    };
    handleEndDateEvent = (event, date) => {
        console.log(event, date);
        this.setState({ endDate: date });
    };
    handleUrgentDateEvent = (event, date) => {
        console.log(event, date);
        this.setState({ urgentDate: date });
    };
}

ContentDrawer.defaultProps = { services: [] };

// Helper functions
        function camelize(str) {
            return str.split(' ').map(function(word){
                return word.charAt(0).toUpperCase() + word.slice(1);
            }).join('');
        }
