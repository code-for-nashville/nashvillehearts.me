/**
 * Created by sada on 10/11/16.
 */

import React from 'react';
import Drawer from 'material-ui/Drawer';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import ContentDrawer from './components/content-drawer';

const styles = {
	tooltitle: {
        color: '#22ABD7'
	}
};

class MainRightDrawer extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = { open: this.props.open };
    }

    doToggleDrawer = (force) => {
        if(typeof force === 'boolean') {
            this.setState({
                open: force
            });
        } else {
            this.setState({
                open: !this.state.open
            });
        }
    }

    render() {

        return (
            <Drawer open={this.state.open} width={300} openSecondary={true}>
                <Toolbar>
                    <ToolbarGroup firstChild={true}>
                        <IconButton onTouchTap={this.doToggleDrawer}><NavigationClose /></IconButton>
                    </ToolbarGroup>
                    <ToolbarGroup>
                        <ToolbarTitle style={styles.tooltitle} text={this.props.title} />
                    </ToolbarGroup>
                </Toolbar>
                <ContentDrawer drawer={this.props.title} contentHandler={this.props.searchHandler}
                               services={this.props.services} data={this.props.data}/>
            </Drawer>
        );

    }
}

MainRightDrawer.defaultProps = { title: "Search", open: false };

export default MainRightDrawer;
