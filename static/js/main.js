/**
 * Created by sada on 10/11/16.
 */

import React from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {white, darkBlack, grey50, blue500, red600, red700, blueGrey900, grey600, grey900} from 'material-ui/styles/colors';
import {darken, fade, emphasize, lighten} from 'material-ui/utils/colorManipulator';
import typography from 'material-ui/styles/typography';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import Dehaze from 'material-ui/svg-icons/image/dehaze';
import Redeem from 'material-ui/svg-icons/action/redeem';
import Event from 'material-ui/svg-icons/action/event';
import Alarm from 'material-ui/svg-icons/action/alarm';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import MainLeftDrawer from './main-left-drawer';
import MainRightDrawer from './main-right-drawer';
import ResourceMap from './resource-map';

const muiTheme = getMuiTheme({
	palette: {
		primary1Color: '#22ABD7',
		primary2Color: blue500,
		primary3Color: blueGrey900,
		accent1Color: '#FF4A43',
		accent2Color: red600,
		accent3Color: grey900,
		textColor: grey600,  //grey50
		secondaryTextColor: fade(grey600, 0.65),  // grey50
		alternateTextColor: darken(grey50, .05), // grey50
		canvasColor: white
	},
	toolbar: {
		backgroundColor: '#282C35',
		color: fade(grey50, 0.80),
		height: 60
	},
	appBar: {
		backgroundColor: '#282C35',
		titleFontWeight: typography.fontWeightLight,
		titleFontSize: 14,
		textColor: '#22ABD7',
		fontSize: 14,
	},
	textField: {
		textColor: grey600
	},
	button: {
		textColor: grey600
	}

});

const styles = {
	container: {
		margin: 0,
	},
	toolbartext: {
		color: grey50
	}
};

let title = "Nashville Hearts Me";
let logo = 'static/images/nav_logo.png';

class Main extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			leftDrawer: false,
			rightDrawer: false,
			page: 'Resources',
			resourceType: 'provider',
			leftDrawerTitle: 'Provider Access',
			services: null,
			data: null
		}
	}

	componentDidMount() {
		this.loadServices();
	}

	handleToggleLeftDrawer = () => {
		this.refs.leftDrawer.doToggleDrawer();
	};

	handleToggleRightDrawer = () => {
		this.refs.rightDrawer.doToggleDrawer();
	};


	loadDataFromServer(url) {
        jQuery.ajax({
            url: url,
            datatype: 'json',
            cache: false,
            success: function(data) {
                this.setState({data: data});
                console.log(data);
            }.bind(this)
        })
    }

    loadServices() {
    	jQuery.ajax({
			url: '/resources/services',
			datatype: 'json',
			cache: false,
			success: function(data) {
				this.setState({services: data});
				console.log("services --> ", data);
			}.bind(this)
		});
	}

    doFindSearchProviders = (search, service) => {
        let url = '/resources/providers/?q=1';
        if(search) {
            url += '&search=' + search;
        }
        if(service) {
            url += '&service=' + service;
        }

        this.loadDataFromServer(url);
    };

	handleSearch = (params) => {
		console.log('main handling search...', params);
		switch(this.state.resourceType){
			case 'provider':
				let search = params.search,
					service = params.service;
				this.doFindSearchProviders(search, service);
				break;
			default:
				break;
		}
	};

	gotoResources = () => {
		this.setState({page: 'Resources', resourceType: 'provider'});
		this.refs.rightDrawer.doToggleDrawer(true);
	};
	gotoEvents = () => {
		this.setState({page: 'Events', resourceType: 'event'});
		this.refs.rightDrawer.doToggleDrawer(true);
	};
	gotoUrgentNeeds = () => {
		this.setState({page: 'Urgent Needs', resourceType: 'need'});
		this.refs.rightDrawer.doToggleDrawer(true);
	};

	render() {
		return (
		    <MuiThemeProvider muiTheme={muiTheme}>
				<div style={styles.container}>
					<Toolbar>
						<ToolbarGroup firstChild={true}>
							<IconButton style={styles.toolbartext} tooltip={this.state.leftDrawerTitle} onTouchTap={this.handleToggleLeftDrawer}><Dehaze/></IconButton>
							<img className="brand" src={logo} />
						</ToolbarGroup>
						<ToolbarGroup>
							<FlatButton
							  label="Resources"
							  labelPosition="before"
							  icon={<Redeem />}
							  onTouchTap={this.gotoResources}
							  style={styles.toolbartext}
							/>
							<FlatButton
							  label="Events"
							  labelPosition="before"
							  icon={<Event/>}
							  onTouchTap={this.gotoEvents}
							  style={styles.toolbartext}
							/>
							<FlatButton
							  label="Urgent Needs"
							  labelPosition="before"
							  icon={<Alarm/>}
							  onTouchTap={this.gotoUrgentNeeds}
							  style={styles.toolbartext}
							/>
						</ToolbarGroup>
						<MainLeftDrawer ref="leftDrawer" title={this.state.leftDrawerTitle} open={this.state.leftDrawer}/>
						<MainRightDrawer ref="rightDrawer" searchHandler={this.handleSearch}
										 title={this.state.page} open={this.state.rightDrawer}
										 services={this.state.services} data={this.props.data} />
					</Toolbar>
					<ResourceMap ref="resourceMap" resourceType={this.state.resourceType}
								 data={this.state.data}/>
				</div>
            </MuiThemeProvider>
        );
	}
}

export default Main;
