from .common import *
from django.utils.timezone import activate


DEBUG = False

# The following settings are for production
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['RDS_DB_NAME'],
        'USER': os.environ['RDS_USERNAME'],
        'PASSWORD': os.environ['RDS_PASSWORD'],
        'HOST': os.environ['RDS_HOSTNAME'],
        'PORT': os.environ['RDS_PORT'],
    }
}

TIME_ZONE = 'America/Chicago'
activate(TIME_ZONE)

NHM_ADMIN_EMAIL = '1city4allpeople@gmail.com'
# NHM_ADMIN_EMAIL = 'webmaster@siggyworks.com'

NHM_TEST_MODE = False

NHM_ROOT_URL = "nmhrc.platform.siggyworks.com"
# NHM_ROOT_URL = "104.236.119.135"
# NHM_ROOT_URL = "127.0.0.1:8000"

NHM_WEB_CLIENT_URL = "nashvillehearts.me"
# NHM_WEB_CLIENT_URL = "nashvilleheartsme.siggyworks.com"
