"""
Django settings for NashvilleHeartsAPI project.

Generated by 'django-admin startproject' using Django 1.9.8.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""

import os
from environ import Environ

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
print(BASE_DIR)

# Config Settings
KEYS_ROOT = os.path.join(BASE_DIR, '_keys')
env_keys = Environ(os.path.join(KEYS_ROOT, 'platform.txt'))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env_keys.get_variable('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = [
    '127.0.0.1',
     # '104.236.119.135',
    '.siggyworks.com',
    '.us-east-1.elasticbeanstalk.com',
    'nashvillehearts.me'
]


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # third party apps
    'rest_framework',
    'rest_framework.authtoken',
    'webpack_loader',
    'corsheaders',
    'storages',

    # local apps
    'nashvillehearts',
]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',

    'corsheaders.middleware.CorsMiddleware',

    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'NashvilleHeartsAPI.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'NashvilleHeartsAPI.wsgi.application'


# Database
# in production and development files


# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# To disable django rest framework's browsable api, uncomment the following:

REST_FRAMEWORK = {
    """
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    )
    """
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    )
}


# CORS-related settings
CORS_ORIGIN_ALLOW_ALL = True  # if set to True, CORS_ORIGIN_WHITELIST is ignored
CORS_ORIGIN_WHITELIST = (
    'siggyworks.com',
    'google.com',
    '.us-east-1.elasticbeanstalk.com',
    'localhost',
    '127.0.0.1'
)
CORS_ALLOW_HEADERS = (
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'Content-Type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
)

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Chicago'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# SECURE_SSL_REDIRECT = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')  # change this to an S3 bucket ...should be 'staticfiles'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
STATIC_URL = '/static/'

# Media files (i.e., uploaded by site users)
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'


# AWS STUFF
# IAM arn:aws:iam::079176206696:user/fiosocial
AWS_STORAGE_BUCKET_NAME = 'cdn-nhmplatform-siggyworks'
AWS_ACCESS_KEY_ID = 'AKIAJPKUUDRB7YLA25QQ'
AWS_SECRET_ACCESS_KEY = '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'

# Tell django-storages that when coming up with the URL for an item in S3 storage, keep
# it simple - just use this domain plus the path. (If this isn't set, things get complicated).
# This controls how the `static` template tag from `staticfiles` gets expanded, if you're using it.
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME

# This is used by the `static` template tag from `static`, if you're using that. Or if anything else
# refers directly to STATIC_URL. So it's safest to always set it.
# STATIC_URL = "https://%s/" % AWS_S3_CUSTOM_DOMAIN

# Tell the staticfiles app to use S3Boto storage when writing the collected static files (when
# you run `collectstatic`).
# STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

MEDIAFILES_LOCATION = 'media'
MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)
DEFAULT_FILE_STORAGE = 'custom_storages.MediaStorage'


# Webpack settings needed for React
WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'bundles/',
        'STATS_FILE': os.path.join(BASE_DIR, 'webpack-stats.json'),
    }
}


# Other Settings
GMAP_KEY = env_keys.get_variable('GMAP_KEY')

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_USE_TLS = True
EMAIL_PORT = 587  # or 465
EMAIL_HOST = 'smtp.sendgrid.net'

EMAIL_HOST_USER = 'apikey'
EMAIL_HOST_PASSWORD = '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'
SERVER_EMAIL = 'Nashville Hearts <webmaster@siggyworks.com>'
DEFAULT_FROM_EMAIL = 'Nashville Hearts <webmaster@siggyworks.com>'

"""
EMAIL_HOST_USER = 'siggyworks'
EMAIL_HOST_PASSWORD = '1r0nP1g30n!'
SERVER_EMAIL = 'Sysadmin <webmaster@siggyworks.com>'
DEFAULT_FROM_EMAIL = 'Webmaster <webmaster@siggyworks.com>'
"""


NHM_ADMIN_EMAIL = '1city4allpeople@gmail.com'
# NHM_ADMIN_EMAIL = 'webmaster@siggyworks.com'

NHM_TEST_MODE = False

NHM_ROOT_URL = "nmhrc.platform.siggyworks.com"
# NHM_ROOT_URL = "104.236.119.135"
# NHM_ROOT_URL = "127.0.0.1:8000"

NHM_WEB_CLIENT_URL = "nashvillehearts.me"
# NHM_WEB_CLIENT_URL = "nashvilleheartsme.siggyworks.com"

